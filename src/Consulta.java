import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import com.google.gson.Gson;

public class Consulta {

	/**
	 * Consulta localidade por CEP
	 * 
	 * @param args
	 * @throws Exception
	 */
	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {

		String cep = "";
		Gson gson = new Gson();

		System.out.print("Entre com o CEP desejado: ");
		cep = new Scanner(System.in).nextLine();
		cep = cep.replace("-", "");

		String urlString = "http://cep.correiocontrol.com.br/" + cep + ".json";

		try {
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestProperty("Request-Method", "GET");
			connection.setDoInput(true);
			connection.setDoOutput(false);

			connection.connect();

			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuffer result = new StringBuffer();

			String s = "";
			while (null != ((s = br.readLine()))) {
				result.append(s);
			}

			br.close();
			Localidade local = gson.fromJson(result.toString(), Localidade.class);
			System.out.println(local.getLogradouro() + ", " + local.getBairro() + ", " + local.getLocalidade() + ", " + local.getUf());

		} catch (MalformedURLException exception) {
			throw new Exception("CEP nao encontrado");
		}
	}
}